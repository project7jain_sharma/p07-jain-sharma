﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_MainMenu4009084430.h"
#include "AssemblyU2DCSharp_RemoveRubble3909082066.h"
#include "AssemblyU2DCSharp_TheStack568733713.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1701[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (MainMenu_t4009084430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[1] = 
{
	MainMenu_t4009084430::get_offset_of_scoreText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (RemoveRubble_t3909082066), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (TheStack_t568733713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[21] = 
{
	TheStack_t568733713::get_offset_of_scoreText_2(),
	TheStack_t568733713::get_offset_of_gameColors_3(),
	TheStack_t568733713::get_offset_of_stackMat_4(),
	TheStack_t568733713::get_offset_of_endPanel_5(),
	0,
	0,
	0,
	0,
	0,
	TheStack_t568733713::get_offset_of_theStack_11(),
	TheStack_t568733713::get_offset_of_stackBounds_12(),
	TheStack_t568733713::get_offset_of_stackIndex_13(),
	TheStack_t568733713::get_offset_of_scoreCount_14(),
	TheStack_t568733713::get_offset_of_combo_15(),
	TheStack_t568733713::get_offset_of_tileTransition_16(),
	TheStack_t568733713::get_offset_of_titleSpeed_17(),
	TheStack_t568733713::get_offset_of_secondaryPosition_18(),
	TheStack_t568733713::get_offset_of_isMovingOfX_19(),
	TheStack_t568733713::get_offset_of_gameOver_20(),
	TheStack_t568733713::get_offset_of_desiredPosition_21(),
	TheStack_t568733713::get_offset_of_lastTilePosition_22(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
