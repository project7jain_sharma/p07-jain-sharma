﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheStack
struct  TheStack_t568733713  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text TheStack::scoreText
	Text_t356221433 * ___scoreText_2;
	// UnityEngine.Color32[] TheStack::gameColors
	Color32U5BU5D_t30278651* ___gameColors_3;
	// UnityEngine.Material TheStack::stackMat
	Material_t193706927 * ___stackMat_4;
	// UnityEngine.GameObject TheStack::endPanel
	GameObject_t1756533147 * ___endPanel_5;
	// UnityEngine.GameObject[] TheStack::theStack
	GameObjectU5BU5D_t3057952154* ___theStack_11;
	// UnityEngine.Vector2 TheStack::stackBounds
	Vector2_t2243707579  ___stackBounds_12;
	// System.Int32 TheStack::stackIndex
	int32_t ___stackIndex_13;
	// System.Int32 TheStack::scoreCount
	int32_t ___scoreCount_14;
	// System.Int32 TheStack::combo
	int32_t ___combo_15;
	// System.Single TheStack::tileTransition
	float ___tileTransition_16;
	// System.Single TheStack::titleSpeed
	float ___titleSpeed_17;
	// System.Single TheStack::secondaryPosition
	float ___secondaryPosition_18;
	// System.Boolean TheStack::isMovingOfX
	bool ___isMovingOfX_19;
	// System.Boolean TheStack::gameOver
	bool ___gameOver_20;
	// UnityEngine.Vector3 TheStack::desiredPosition
	Vector3_t2243707580  ___desiredPosition_21;
	// UnityEngine.Vector3 TheStack::lastTilePosition
	Vector3_t2243707580  ___lastTilePosition_22;

public:
	inline static int32_t get_offset_of_scoreText_2() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___scoreText_2)); }
	inline Text_t356221433 * get_scoreText_2() const { return ___scoreText_2; }
	inline Text_t356221433 ** get_address_of_scoreText_2() { return &___scoreText_2; }
	inline void set_scoreText_2(Text_t356221433 * value)
	{
		___scoreText_2 = value;
		Il2CppCodeGenWriteBarrier(&___scoreText_2, value);
	}

	inline static int32_t get_offset_of_gameColors_3() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___gameColors_3)); }
	inline Color32U5BU5D_t30278651* get_gameColors_3() const { return ___gameColors_3; }
	inline Color32U5BU5D_t30278651** get_address_of_gameColors_3() { return &___gameColors_3; }
	inline void set_gameColors_3(Color32U5BU5D_t30278651* value)
	{
		___gameColors_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameColors_3, value);
	}

	inline static int32_t get_offset_of_stackMat_4() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___stackMat_4)); }
	inline Material_t193706927 * get_stackMat_4() const { return ___stackMat_4; }
	inline Material_t193706927 ** get_address_of_stackMat_4() { return &___stackMat_4; }
	inline void set_stackMat_4(Material_t193706927 * value)
	{
		___stackMat_4 = value;
		Il2CppCodeGenWriteBarrier(&___stackMat_4, value);
	}

	inline static int32_t get_offset_of_endPanel_5() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___endPanel_5)); }
	inline GameObject_t1756533147 * get_endPanel_5() const { return ___endPanel_5; }
	inline GameObject_t1756533147 ** get_address_of_endPanel_5() { return &___endPanel_5; }
	inline void set_endPanel_5(GameObject_t1756533147 * value)
	{
		___endPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___endPanel_5, value);
	}

	inline static int32_t get_offset_of_theStack_11() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___theStack_11)); }
	inline GameObjectU5BU5D_t3057952154* get_theStack_11() const { return ___theStack_11; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_theStack_11() { return &___theStack_11; }
	inline void set_theStack_11(GameObjectU5BU5D_t3057952154* value)
	{
		___theStack_11 = value;
		Il2CppCodeGenWriteBarrier(&___theStack_11, value);
	}

	inline static int32_t get_offset_of_stackBounds_12() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___stackBounds_12)); }
	inline Vector2_t2243707579  get_stackBounds_12() const { return ___stackBounds_12; }
	inline Vector2_t2243707579 * get_address_of_stackBounds_12() { return &___stackBounds_12; }
	inline void set_stackBounds_12(Vector2_t2243707579  value)
	{
		___stackBounds_12 = value;
	}

	inline static int32_t get_offset_of_stackIndex_13() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___stackIndex_13)); }
	inline int32_t get_stackIndex_13() const { return ___stackIndex_13; }
	inline int32_t* get_address_of_stackIndex_13() { return &___stackIndex_13; }
	inline void set_stackIndex_13(int32_t value)
	{
		___stackIndex_13 = value;
	}

	inline static int32_t get_offset_of_scoreCount_14() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___scoreCount_14)); }
	inline int32_t get_scoreCount_14() const { return ___scoreCount_14; }
	inline int32_t* get_address_of_scoreCount_14() { return &___scoreCount_14; }
	inline void set_scoreCount_14(int32_t value)
	{
		___scoreCount_14 = value;
	}

	inline static int32_t get_offset_of_combo_15() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___combo_15)); }
	inline int32_t get_combo_15() const { return ___combo_15; }
	inline int32_t* get_address_of_combo_15() { return &___combo_15; }
	inline void set_combo_15(int32_t value)
	{
		___combo_15 = value;
	}

	inline static int32_t get_offset_of_tileTransition_16() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___tileTransition_16)); }
	inline float get_tileTransition_16() const { return ___tileTransition_16; }
	inline float* get_address_of_tileTransition_16() { return &___tileTransition_16; }
	inline void set_tileTransition_16(float value)
	{
		___tileTransition_16 = value;
	}

	inline static int32_t get_offset_of_titleSpeed_17() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___titleSpeed_17)); }
	inline float get_titleSpeed_17() const { return ___titleSpeed_17; }
	inline float* get_address_of_titleSpeed_17() { return &___titleSpeed_17; }
	inline void set_titleSpeed_17(float value)
	{
		___titleSpeed_17 = value;
	}

	inline static int32_t get_offset_of_secondaryPosition_18() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___secondaryPosition_18)); }
	inline float get_secondaryPosition_18() const { return ___secondaryPosition_18; }
	inline float* get_address_of_secondaryPosition_18() { return &___secondaryPosition_18; }
	inline void set_secondaryPosition_18(float value)
	{
		___secondaryPosition_18 = value;
	}

	inline static int32_t get_offset_of_isMovingOfX_19() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___isMovingOfX_19)); }
	inline bool get_isMovingOfX_19() const { return ___isMovingOfX_19; }
	inline bool* get_address_of_isMovingOfX_19() { return &___isMovingOfX_19; }
	inline void set_isMovingOfX_19(bool value)
	{
		___isMovingOfX_19 = value;
	}

	inline static int32_t get_offset_of_gameOver_20() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___gameOver_20)); }
	inline bool get_gameOver_20() const { return ___gameOver_20; }
	inline bool* get_address_of_gameOver_20() { return &___gameOver_20; }
	inline void set_gameOver_20(bool value)
	{
		___gameOver_20 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_21() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___desiredPosition_21)); }
	inline Vector3_t2243707580  get_desiredPosition_21() const { return ___desiredPosition_21; }
	inline Vector3_t2243707580 * get_address_of_desiredPosition_21() { return &___desiredPosition_21; }
	inline void set_desiredPosition_21(Vector3_t2243707580  value)
	{
		___desiredPosition_21 = value;
	}

	inline static int32_t get_offset_of_lastTilePosition_22() { return static_cast<int32_t>(offsetof(TheStack_t568733713, ___lastTilePosition_22)); }
	inline Vector3_t2243707580  get_lastTilePosition_22() const { return ___lastTilePosition_22; }
	inline Vector3_t2243707580 * get_address_of_lastTilePosition_22() { return &___lastTilePosition_22; }
	inline void set_lastTilePosition_22(Vector3_t2243707580  value)
	{
		___lastTilePosition_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
