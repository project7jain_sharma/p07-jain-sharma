﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_MainMenu4009084430.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_RemoveRubble3909082066.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_TheStack568733713.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_PrimitiveType2454390065.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// MainMenu
struct MainMenu_t4009084430;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.String
struct String_t;
// RemoveRubble
struct RemoveRubble_t3909082066;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Object
struct Object_t1021602117;
// TheStack
struct TheStack_t568733713;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// System.Object
struct Il2CppObject;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
extern Il2CppCodeGenString* _stringLiteral2247021248;
extern const uint32_t MainMenu_start_m371673979_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2328219732;
extern const uint32_t MainMenu_ToGame_m3162949598_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RemoveRubble_OnCollisonEnter_m3882182186_MetadataUsageId;
extern Il2CppClass* Color32U5BU5D_t30278651_il2cpp_TypeInfo_var;
extern const uint32_t TheStack__ctor_m129042520_MetadataUsageId;
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839_MethodInfo_var;
extern const uint32_t TheStack_Start_m1327114128_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t TheStack_Update_m2407572313_MetadataUsageId;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632_MethodInfo_var;
extern const uint32_t TheStack_CreateRubble_m2807183450_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TheStack_MoveTile_m4196941625_MetadataUsageId;
extern const uint32_t TheStack_SpawnTitle_m590331931_MetadataUsageId;
extern const uint32_t TheStack_PlaceTitle_m3582593275_MetadataUsageId;
extern const uint32_t TheStack_ColorMesh_m995884212_MetadataUsageId;
extern const uint32_t TheStack_EndGame_m2402922789_MetadataUsageId;

// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color32_t874517518  m_Items[1];

public:
	inline Color32_t874517518  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t874517518 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t874517518  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t874517518  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t874517518 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t874517518  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m2889062785 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1619949821 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern "C"  GameObject_t1756533147 * Collision_get_gameObject_m1370363400 (Collision_t2876846408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m881385315 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshFilter>()
#define GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839(__this, method) ((  MeshFilter_t3026937449 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_mesh_m977520135 (MeshFilter_t3026937449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TheStack::ColorMesh(UnityEngine.Mesh)
extern "C"  void TheStack_ColorMesh_m995884212 (TheStack_t568733713 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m47917805 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TheStack::PlaceTitle()
extern "C"  bool TheStack_PlaceTitle_m3582593275 (TheStack_t568733713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TheStack::SpawnTitle()
extern "C"  void TheStack_SpawnTitle_m590331931 (TheStack_t568733713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TheStack::EndGame()
extern "C"  void TheStack_EndGame_m2402922789 (TheStack_t568733713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TheStack::MoveTile()
extern "C"  void TheStack_MoveTile_m4196941625 (TheStack_t568733713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_Lerp_m2935648359 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern "C"  GameObject_t1756533147 * GameObject_CreatePrimitive_m973880764 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1026930133 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2325460848 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
#define GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(__this, method) ((  Rigidbody_t4233889191 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632(__this, method) ((  MeshRenderer_t1268241104 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1053097112 (Renderer_t257310565 * __this, Material_t193706927 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2243707580  Transform_get_localPosition_m2533925116 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t2243707580  Vector3_get_down_m2372302126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2243707580  Transform_get_localScale_m3074381503 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TheStack::CreateRubble(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void TheStack_CreateRubble_m2807183450 (TheStack_t568733713 * __this, Vector3_t2243707580  ___pos0, Vector3_t2243707580  ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_vertices_m626989480 (Mesh_t1356156583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 TheStack::Lerp4(UnityEngine.Color32,UnityEngine.Color32,UnityEngine.Color32,UnityEngine.Color32,System.Single)
extern "C"  Color32_t874517518  TheStack_Lerp4_m723530364 (TheStack_t568733713 * __this, Color32_t874517518  ___a0, Color32_t874517518  ___b1, Color32_t874517518  ___c2, Color32_t874517518  ___d3, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
extern "C"  void Mesh_set_colors32_m1066151745 (Mesh_t1356156583 * __this, Color32U5BU5D_t30278651* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t2020392075  Color32_op_Implicit_m889975790 (Il2CppObject * __this /* static, unused */, Color32_t874517518  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2020392075  Color_Lerp_m3323752807 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t874517518  Color32_op_Implicit_m624191464 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m3351928596 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu::.ctor()
extern "C"  void MainMenu__ctor_m2536434407 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::start()
extern "C"  void MainMenu_start_m371673979 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_start_m371673979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Text_t356221433 * L_0 = __this->get_scoreText_2();
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2247021248, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void MainMenu::ToGame()
extern "C"  void MainMenu_ToGame_m3162949598 (MainMenu_t4009084430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_ToGame_m3162949598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2328219732, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RemoveRubble::.ctor()
extern "C"  void RemoveRubble__ctor_m878229487 (RemoveRubble_t3909082066 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RemoveRubble::OnCollisonEnter(UnityEngine.Collision)
extern "C"  void RemoveRubble_OnCollisonEnter_m3882182186 (RemoveRubble_t3909082066 * __this, Collision_t2876846408 * ___col0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoveRubble_OnCollisonEnter_m3882182186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collision_t2876846408 * L_0 = ___col0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Collision_get_gameObject_m1370363400(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheStack::.ctor()
extern "C"  void TheStack__ctor_m129042520 (TheStack_t568733713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack__ctor_m129042520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gameColors_3(((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)4)));
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (3.5f), (3.5f), /*hidden argument*/NULL);
		__this->set_stackBounds_12(L_0);
		__this->set_titleSpeed_17((2.5f));
		__this->set_isMovingOfX_19((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheStack::Start()
extern "C"  void TheStack_Start_m1327114128 (TheStack_t568733713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_Start_m1327114128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m881385315(L_0, /*hidden argument*/NULL);
		__this->set_theStack_11(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_1)));
		V_0 = 0;
		goto IL_0052;
	}

IL_001d:
	{
		GameObjectU5BU5D_t3057952154* L_2 = __this->get_theStack_11();
		int32_t L_3 = V_0;
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_7);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (GameObject_t1756533147 *)L_7);
		GameObjectU5BU5D_t3057952154* L_8 = __this->get_theStack_11();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		GameObject_t1756533147 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		MeshFilter_t3026937449 * L_12 = GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839(L_11, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839_MethodInfo_var);
		NullCheck(L_12);
		Mesh_t1356156583 * L_13 = MeshFilter_get_mesh_m977520135(L_12, /*hidden argument*/NULL);
		TheStack_ColorMesh_m995884212(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_15 = V_0;
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = Transform_get_childCount_m881385315(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_001d;
		}
	}
	{
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = Transform_get_childCount_m881385315(L_18, /*hidden argument*/NULL);
		__this->set_stackIndex_13(((int32_t)((int32_t)L_19-(int32_t)1)));
		return;
	}
}
// System.Void TheStack::Update()
extern "C"  void TheStack_Update_m2407572313 (TheStack_t568733713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_Update_m2407572313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_gameOver_20();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005d;
		}
	}
	{
		bool L_2 = TheStack_PlaceTitle_m3582593275(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0057;
		}
	}
	{
		TheStack_SpawnTitle_m590331931(__this, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_scoreCount_14();
		__this->set_scoreCount_14(((int32_t)((int32_t)L_3+(int32_t)1)));
		Text_t356221433 * L_4 = __this->get_scoreText_2();
		int32_t* L_5 = __this->get_address_of_scoreCount_14();
		String_t* L_6 = Int32_ToString_m2960866144(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_6);
		goto IL_005d;
	}

IL_0057:
	{
		TheStack_EndGame_m2402922789(__this, /*hidden argument*/NULL);
	}

IL_005d:
	{
		TheStack_MoveTile_m4196941625(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = __this->get_desiredPosition_21();
		float L_11 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_9, L_10, ((float)((float)(5.0f)*(float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_position_m2469242620(L_7, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheStack::CreateRubble(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void TheStack_CreateRubble_m2807183450 (TheStack_t568733713 * __this, Vector3_t2243707580  ___pos0, Vector3_t2243707580  ___scale1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_CreateRubble_m2807183450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_CreatePrimitive_m973880764(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = ___pos0;
		NullCheck(L_2);
		Transform_set_localPosition_m1026930133(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = ___scale1;
		NullCheck(L_5);
		Transform_set_localScale_m2325460848(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(L_7, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var);
		GameObject_t1756533147 * L_8 = V_0;
		NullCheck(L_8);
		MeshRenderer_t1268241104 * L_9 = GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632(L_8, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632_MethodInfo_var);
		Material_t193706927 * L_10 = __this->get_stackMat_4();
		NullCheck(L_9);
		Renderer_set_material_m1053097112(L_9, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = V_0;
		NullCheck(L_11);
		MeshFilter_t3026937449 * L_12 = GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839(L_11, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839_MethodInfo_var);
		NullCheck(L_12);
		Mesh_t1356156583 * L_13 = MeshFilter_get_mesh_m977520135(L_12, /*hidden argument*/NULL);
		TheStack_ColorMesh_m995884212(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TheStack::MoveTile()
extern "C"  void TheStack_MoveTile_m4196941625 (TheStack_t568733713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_MoveTile_m4196941625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_tileTransition_16();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_titleSpeed_17();
		__this->set_tileTransition_16(((float)((float)L_0+(float)((float)((float)L_1*(float)L_2)))));
		bool L_3 = __this->get_isMovingOfX_19();
		if (!L_3)
		{
			goto IL_0063;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_theStack_11();
		int32_t L_5 = __this->get_stackIndex_13();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_t1756533147 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_tileTransition_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = sinf(L_9);
		int32_t L_11 = __this->get_scoreCount_14();
		float L_12 = __this->get_secondaryPosition_18();
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, ((float)((float)L_10*(float)(3.5f))), (((float)((float)L_11))), L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localPosition_m1026930133(L_8, L_13, /*hidden argument*/NULL);
		goto IL_009d;
	}

IL_0063:
	{
		GameObjectU5BU5D_t3057952154* L_14 = __this->get_theStack_11();
		int32_t L_15 = __this->get_stackIndex_13();
		NullCheck(L_14);
		int32_t L_16 = L_15;
		GameObject_t1756533147 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = GameObject_get_transform_m909382139(L_17, /*hidden argument*/NULL);
		float L_19 = __this->get_secondaryPosition_18();
		int32_t L_20 = __this->get_scoreCount_14();
		float L_21 = __this->get_tileTransition_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_22 = sinf(L_21);
		Vector3_t2243707580  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m2638739322(&L_23, L_19, (((float)((float)L_20))), ((float)((float)L_22*(float)(3.5f))), /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localPosition_m1026930133(L_18, L_23, /*hidden argument*/NULL);
	}

IL_009d:
	{
		return;
	}
}
// System.Void TheStack::SpawnTitle()
extern "C"  void TheStack_SpawnTitle_m590331931 (TheStack_t568733713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_SpawnTitle_m590331931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_theStack_11();
		int32_t L_1 = __this->get_stackIndex_13();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1756533147 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_localPosition_m2533925116(L_4, /*hidden argument*/NULL);
		__this->set_lastTilePosition_22(L_5);
		int32_t L_6 = __this->get_stackIndex_13();
		__this->set_stackIndex_13(((int32_t)((int32_t)L_6-(int32_t)1)));
		int32_t L_7 = __this->get_stackIndex_13();
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_004a;
		}
	}
	{
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m881385315(L_8, /*hidden argument*/NULL);
		__this->set_stackIndex_13(((int32_t)((int32_t)L_9-(int32_t)1)));
	}

IL_004a:
	{
		Vector3_t2243707580  L_10 = Vector3_get_down_m2372302126(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_scoreCount_14();
		Vector3_t2243707580  L_12 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_10, (((float)((float)L_11))), /*hidden argument*/NULL);
		__this->set_desiredPosition_21(L_12);
		GameObjectU5BU5D_t3057952154* L_13 = __this->get_theStack_11();
		int32_t L_14 = __this->get_stackIndex_13();
		NullCheck(L_13);
		int32_t L_15 = L_14;
		GameObject_t1756533147 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		int32_t L_18 = __this->get_scoreCount_14();
		Vector3_t2243707580  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2638739322(&L_19, (0.0f), (((float)((float)L_18))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localPosition_m1026930133(L_17, L_19, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_20 = __this->get_theStack_11();
		int32_t L_21 = __this->get_stackIndex_13();
		NullCheck(L_20);
		int32_t L_22 = L_21;
		GameObject_t1756533147 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_25 = __this->get_address_of_stackBounds_12();
		float L_26 = L_25->get_x_0();
		Vector2_t2243707579 * L_27 = __this->get_address_of_stackBounds_12();
		float L_28 = L_27->get_y_1();
		Vector3_t2243707580  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector3__ctor_m2638739322(&L_29, L_26, (1.0f), L_28, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localScale_m2325460848(L_24, L_29, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_30 = __this->get_theStack_11();
		int32_t L_31 = __this->get_stackIndex_13();
		NullCheck(L_30);
		int32_t L_32 = L_31;
		GameObject_t1756533147 * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		MeshFilter_t3026937449 * L_34 = GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839(L_33, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3026937449_m1019809839_MethodInfo_var);
		NullCheck(L_34);
		Mesh_t1356156583 * L_35 = MeshFilter_get_mesh_m977520135(L_34, /*hidden argument*/NULL);
		TheStack_ColorMesh_m995884212(__this, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TheStack::PlaceTitle()
extern "C"  bool TheStack_PlaceTitle_m3582593275 (TheStack_t568733713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_PlaceTitle_m3582593275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float V_17 = 0.0f;
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	float V_19 = 0.0f;
	Vector3_t2243707580  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t2243707580  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Vector3_t2243707580  V_22;
	memset(&V_22, 0, sizeof(V_22));
	Vector3_t2243707580  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_t2243707580  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Vector3_t2243707580  V_25;
	memset(&V_25, 0, sizeof(V_25));
	Vector3_t2243707580  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t2243707580  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Vector3_t2243707580  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t2243707580  V_29;
	memset(&V_29, 0, sizeof(V_29));
	Vector3_t2243707580  V_30;
	memset(&V_30, 0, sizeof(V_30));
	TheStack_t568733713 * G_B6_0 = NULL;
	TheStack_t568733713 * G_B5_0 = NULL;
	float G_B7_0 = 0.0f;
	TheStack_t568733713 * G_B7_1 = NULL;
	float G_B23_0 = 0.0f;
	float G_B23_1 = 0.0f;
	TheStack_t568733713 * G_B23_2 = NULL;
	float G_B22_0 = 0.0f;
	float G_B22_1 = 0.0f;
	TheStack_t568733713 * G_B22_2 = NULL;
	float G_B24_0 = 0.0f;
	float G_B24_1 = 0.0f;
	float G_B24_2 = 0.0f;
	TheStack_t568733713 * G_B24_3 = NULL;
	TheStack_t568733713 * G_B28_0 = NULL;
	TheStack_t568733713 * G_B27_0 = NULL;
	float G_B29_0 = 0.0f;
	TheStack_t568733713 * G_B29_1 = NULL;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_theStack_11();
		int32_t L_1 = __this->get_stackIndex_13();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1756533147 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = __this->get_isMovingOfX_19();
		if (!L_5)
		{
			goto IL_02a9;
		}
	}
	{
		Vector3_t2243707580 * L_6 = __this->get_address_of_lastTilePosition_22();
		float L_7 = L_6->get_x_1();
		Transform_t3275118058 * L_8 = V_0;
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_x_1();
		V_1 = ((float)((float)L_7-(float)L_10));
		float L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_12 = fabsf(L_11);
		if ((!(((float)L_12) > ((float)(0.1f)))))
		{
			goto IL_01ac;
		}
	}
	{
		__this->set_combo_15(0);
		Vector2_t2243707579 * L_13 = __this->get_address_of_stackBounds_12();
		Vector2_t2243707579 * L_14 = L_13;
		float L_15 = L_14->get_x_0();
		float L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_17 = fabsf(L_16);
		L_14->set_x_0(((float)((float)L_15-(float)L_17)));
		Vector2_t2243707579 * L_18 = __this->get_address_of_stackBounds_12();
		float L_19 = L_18->get_x_0();
		if ((!(((float)L_19) <= ((float)(0.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		return (bool)0;
	}

IL_007f:
	{
		Vector3_t2243707580 * L_20 = __this->get_address_of_lastTilePosition_22();
		float L_21 = L_20->get_x_1();
		Transform_t3275118058 * L_22 = V_0;
		NullCheck(L_22);
		Vector3_t2243707580  L_23 = Transform_get_localPosition_m2533925116(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		float L_24 = (&V_4)->get_x_1();
		V_3 = ((float)((float)L_21+(float)((float)((float)L_24/(float)(2.0f)))));
		Transform_t3275118058 * L_25 = V_0;
		Vector2_t2243707579 * L_26 = __this->get_address_of_stackBounds_12();
		float L_27 = L_26->get_x_0();
		Vector2_t2243707579 * L_28 = __this->get_address_of_stackBounds_12();
		float L_29 = L_28->get_y_1();
		Vector3_t2243707580  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m2638739322(&L_30, L_27, (1.0f), L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localScale_m2325460848(L_25, L_30, /*hidden argument*/NULL);
		Transform_t3275118058 * L_31 = V_0;
		NullCheck(L_31);
		Vector3_t2243707580  L_32 = Transform_get_position_m1104419803(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		float L_33 = (&V_5)->get_x_1();
		G_B5_0 = __this;
		if ((!(((float)L_33) > ((float)(0.0f)))))
		{
			G_B6_0 = __this;
			goto IL_010b;
		}
	}
	{
		Transform_t3275118058 * L_34 = V_0;
		NullCheck(L_34);
		Vector3_t2243707580  L_35 = Transform_get_position_m1104419803(L_34, /*hidden argument*/NULL);
		V_6 = L_35;
		float L_36 = (&V_6)->get_x_1();
		Transform_t3275118058 * L_37 = V_0;
		NullCheck(L_37);
		Vector3_t2243707580  L_38 = Transform_get_localScale_m3074381503(L_37, /*hidden argument*/NULL);
		V_7 = L_38;
		float L_39 = (&V_7)->get_x_1();
		G_B7_0 = ((float)((float)L_36+(float)((float)((float)L_39/(float)(2.0f)))));
		G_B7_1 = G_B5_0;
		goto IL_0130;
	}

IL_010b:
	{
		Transform_t3275118058 * L_40 = V_0;
		NullCheck(L_40);
		Vector3_t2243707580  L_41 = Transform_get_position_m1104419803(L_40, /*hidden argument*/NULL);
		V_8 = L_41;
		float L_42 = (&V_8)->get_x_1();
		Transform_t3275118058 * L_43 = V_0;
		NullCheck(L_43);
		Vector3_t2243707580  L_44 = Transform_get_localScale_m3074381503(L_43, /*hidden argument*/NULL);
		V_9 = L_44;
		float L_45 = (&V_9)->get_x_1();
		G_B7_0 = ((float)((float)L_42-(float)((float)((float)L_45/(float)(2.0f)))));
		G_B7_1 = G_B6_0;
	}

IL_0130:
	{
		Transform_t3275118058 * L_46 = V_0;
		NullCheck(L_46);
		Vector3_t2243707580  L_47 = Transform_get_position_m1104419803(L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		float L_48 = (&V_10)->get_y_2();
		Transform_t3275118058 * L_49 = V_0;
		NullCheck(L_49);
		Vector3_t2243707580  L_50 = Transform_get_position_m1104419803(L_49, /*hidden argument*/NULL);
		V_11 = L_50;
		float L_51 = (&V_11)->get_z_3();
		Vector3_t2243707580  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector3__ctor_m2638739322(&L_52, G_B7_0, L_48, L_51, /*hidden argument*/NULL);
		float L_53 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_54 = fabsf(L_53);
		Transform_t3275118058 * L_55 = V_0;
		NullCheck(L_55);
		Vector3_t2243707580  L_56 = Transform_get_localScale_m3074381503(L_55, /*hidden argument*/NULL);
		V_12 = L_56;
		float L_57 = (&V_12)->get_x_1();
		Vector3_t2243707580  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Vector3__ctor_m2638739322(&L_58, L_54, (1.0f), L_57, /*hidden argument*/NULL);
		NullCheck(G_B7_1);
		TheStack_CreateRubble_m2807183450(G_B7_1, L_52, L_58, /*hidden argument*/NULL);
		Transform_t3275118058 * L_59 = V_0;
		float L_60 = V_3;
		Vector3_t2243707580 * L_61 = __this->get_address_of_lastTilePosition_22();
		float L_62 = L_61->get_x_1();
		int32_t L_63 = __this->get_scoreCount_14();
		Vector3_t2243707580 * L_64 = __this->get_address_of_lastTilePosition_22();
		float L_65 = L_64->get_z_3();
		Vector3_t2243707580  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Vector3__ctor_m2638739322(&L_66, ((float)((float)L_60-(float)((float)((float)L_62/(float)(2.0f))))), (((float)((float)L_63))), L_65, /*hidden argument*/NULL);
		NullCheck(L_59);
		Transform_set_localPosition_m1026930133(L_59, L_66, /*hidden argument*/NULL);
		goto IL_02a4;
	}

IL_01ac:
	{
		int32_t L_67 = __this->get_combo_15();
		if ((((int32_t)L_67) <= ((int32_t)3)))
		{
			goto IL_026e;
		}
	}
	{
		Vector2_t2243707579 * L_68 = __this->get_address_of_stackBounds_12();
		Vector2_t2243707579 * L_69 = L_68;
		float L_70 = L_69->get_x_0();
		L_69->set_x_0(((float)((float)L_70+(float)(0.25f))));
		Vector2_t2243707579 * L_71 = __this->get_address_of_stackBounds_12();
		float L_72 = L_71->get_x_0();
		if ((!(((float)L_72) > ((float)(3.5f)))))
		{
			goto IL_01f4;
		}
	}
	{
		Vector2_t2243707579 * L_73 = __this->get_address_of_stackBounds_12();
		L_73->set_x_0((3.5f));
	}

IL_01f4:
	{
		Vector3_t2243707580 * L_74 = __this->get_address_of_lastTilePosition_22();
		float L_75 = L_74->get_x_1();
		Transform_t3275118058 * L_76 = V_0;
		NullCheck(L_76);
		Vector3_t2243707580  L_77 = Transform_get_localPosition_m2533925116(L_76, /*hidden argument*/NULL);
		V_14 = L_77;
		float L_78 = (&V_14)->get_x_1();
		V_13 = ((float)((float)L_75+(float)((float)((float)L_78/(float)(2.0f)))));
		Transform_t3275118058 * L_79 = V_0;
		Vector2_t2243707579 * L_80 = __this->get_address_of_stackBounds_12();
		float L_81 = L_80->get_x_0();
		Vector2_t2243707579 * L_82 = __this->get_address_of_stackBounds_12();
		float L_83 = L_82->get_y_1();
		Vector3_t2243707580  L_84;
		memset(&L_84, 0, sizeof(L_84));
		Vector3__ctor_m2638739322(&L_84, L_81, (1.0f), L_83, /*hidden argument*/NULL);
		NullCheck(L_79);
		Transform_set_localScale_m2325460848(L_79, L_84, /*hidden argument*/NULL);
		Transform_t3275118058 * L_85 = V_0;
		float L_86 = V_13;
		Vector3_t2243707580 * L_87 = __this->get_address_of_lastTilePosition_22();
		float L_88 = L_87->get_x_1();
		int32_t L_89 = __this->get_scoreCount_14();
		Vector3_t2243707580 * L_90 = __this->get_address_of_lastTilePosition_22();
		float L_91 = L_90->get_z_3();
		Vector3_t2243707580  L_92;
		memset(&L_92, 0, sizeof(L_92));
		Vector3__ctor_m2638739322(&L_92, ((float)((float)L_86-(float)((float)((float)L_88/(float)(2.0f))))), (((float)((float)L_89))), L_91, /*hidden argument*/NULL);
		NullCheck(L_85);
		Transform_set_localPosition_m1026930133(L_85, L_92, /*hidden argument*/NULL);
	}

IL_026e:
	{
		int32_t L_93 = __this->get_combo_15();
		__this->set_combo_15(((int32_t)((int32_t)L_93+(int32_t)1)));
		Transform_t3275118058 * L_94 = V_0;
		Vector3_t2243707580 * L_95 = __this->get_address_of_lastTilePosition_22();
		float L_96 = L_95->get_x_1();
		int32_t L_97 = __this->get_scoreCount_14();
		Vector3_t2243707580 * L_98 = __this->get_address_of_lastTilePosition_22();
		float L_99 = L_98->get_z_3();
		Vector3_t2243707580  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Vector3__ctor_m2638739322(&L_100, L_96, (((float)((float)L_97))), L_99, /*hidden argument*/NULL);
		NullCheck(L_94);
		Transform_set_localPosition_m1026930133(L_94, L_100, /*hidden argument*/NULL);
	}

IL_02a4:
	{
		goto IL_0536;
	}

IL_02a9:
	{
		Vector3_t2243707580 * L_101 = __this->get_address_of_lastTilePosition_22();
		float L_102 = L_101->get_z_3();
		Transform_t3275118058 * L_103 = V_0;
		NullCheck(L_103);
		Vector3_t2243707580  L_104 = Transform_get_position_m1104419803(L_103, /*hidden argument*/NULL);
		V_16 = L_104;
		float L_105 = (&V_16)->get_z_3();
		V_15 = ((float)((float)L_102-(float)L_105));
		float L_106 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_107 = fabsf(L_106);
		if ((!(((float)L_107) > ((float)(0.1f)))))
		{
			goto IL_038d;
		}
	}
	{
		__this->set_combo_15(0);
		Vector2_t2243707579 * L_108 = __this->get_address_of_stackBounds_12();
		Vector2_t2243707579 * L_109 = L_108;
		float L_110 = L_109->get_y_1();
		float L_111 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_112 = fabsf(L_111);
		L_109->set_y_1(((float)((float)L_110-(float)L_112)));
		Vector2_t2243707579 * L_113 = __this->get_address_of_stackBounds_12();
		float L_114 = L_113->get_y_1();
		if ((!(((float)L_114) <= ((float)(0.0f)))))
		{
			goto IL_030e;
		}
	}
	{
		return (bool)0;
	}

IL_030e:
	{
		Vector3_t2243707580 * L_115 = __this->get_address_of_lastTilePosition_22();
		float L_116 = L_115->get_z_3();
		Transform_t3275118058 * L_117 = V_0;
		NullCheck(L_117);
		Vector3_t2243707580  L_118 = Transform_get_localPosition_m2533925116(L_117, /*hidden argument*/NULL);
		V_18 = L_118;
		float L_119 = (&V_18)->get_z_3();
		V_17 = ((float)((float)L_116+(float)((float)((float)L_119/(float)(2.0f)))));
		Transform_t3275118058 * L_120 = V_0;
		Vector2_t2243707579 * L_121 = __this->get_address_of_stackBounds_12();
		float L_122 = L_121->get_x_0();
		Vector2_t2243707579 * L_123 = __this->get_address_of_stackBounds_12();
		float L_124 = L_123->get_y_1();
		Vector3_t2243707580  L_125;
		memset(&L_125, 0, sizeof(L_125));
		Vector3__ctor_m2638739322(&L_125, L_122, (1.0f), L_124, /*hidden argument*/NULL);
		NullCheck(L_120);
		Transform_set_localScale_m2325460848(L_120, L_125, /*hidden argument*/NULL);
		Transform_t3275118058 * L_126 = V_0;
		Vector3_t2243707580 * L_127 = __this->get_address_of_lastTilePosition_22();
		float L_128 = L_127->get_x_1();
		int32_t L_129 = __this->get_scoreCount_14();
		float L_130 = V_17;
		Vector3_t2243707580 * L_131 = __this->get_address_of_lastTilePosition_22();
		float L_132 = L_131->get_z_3();
		Vector3_t2243707580  L_133;
		memset(&L_133, 0, sizeof(L_133));
		Vector3__ctor_m2638739322(&L_133, L_128, (((float)((float)L_129))), ((float)((float)L_130-(float)((float)((float)L_132/(float)(2.0f))))), /*hidden argument*/NULL);
		NullCheck(L_126);
		Transform_set_localPosition_m1026930133(L_126, L_133, /*hidden argument*/NULL);
		goto IL_0536;
	}

IL_038d:
	{
		int32_t L_134 = __this->get_combo_15();
		if ((((int32_t)L_134) <= ((int32_t)3)))
		{
			goto IL_0500;
		}
	}
	{
		Vector2_t2243707579 * L_135 = __this->get_address_of_stackBounds_12();
		Vector2_t2243707579 * L_136 = L_135;
		float L_137 = L_136->get_y_1();
		L_136->set_y_1(((float)((float)L_137+(float)(0.25f))));
		Vector2_t2243707579 * L_138 = __this->get_address_of_stackBounds_12();
		float L_139 = L_138->get_y_1();
		if ((!(((float)L_139) > ((float)(3.5f)))))
		{
			goto IL_03d5;
		}
	}
	{
		Vector2_t2243707579 * L_140 = __this->get_address_of_stackBounds_12();
		L_140->set_y_1((3.5f));
	}

IL_03d5:
	{
		Vector3_t2243707580 * L_141 = __this->get_address_of_lastTilePosition_22();
		float L_142 = L_141->get_z_3();
		Transform_t3275118058 * L_143 = V_0;
		NullCheck(L_143);
		Vector3_t2243707580  L_144 = Transform_get_localPosition_m2533925116(L_143, /*hidden argument*/NULL);
		V_20 = L_144;
		float L_145 = (&V_20)->get_z_3();
		V_19 = ((float)((float)L_142+(float)((float)((float)L_145/(float)(2.0f)))));
		Transform_t3275118058 * L_146 = V_0;
		Vector2_t2243707579 * L_147 = __this->get_address_of_stackBounds_12();
		float L_148 = L_147->get_x_0();
		Vector2_t2243707579 * L_149 = __this->get_address_of_stackBounds_12();
		float L_150 = L_149->get_y_1();
		Vector3_t2243707580  L_151;
		memset(&L_151, 0, sizeof(L_151));
		Vector3__ctor_m2638739322(&L_151, L_148, (1.0f), L_150, /*hidden argument*/NULL);
		NullCheck(L_146);
		Transform_set_localScale_m2325460848(L_146, L_151, /*hidden argument*/NULL);
		Transform_t3275118058 * L_152 = V_0;
		NullCheck(L_152);
		Vector3_t2243707580  L_153 = Transform_get_position_m1104419803(L_152, /*hidden argument*/NULL);
		V_21 = L_153;
		float L_154 = (&V_21)->get_x_1();
		Transform_t3275118058 * L_155 = V_0;
		NullCheck(L_155);
		Vector3_t2243707580  L_156 = Transform_get_position_m1104419803(L_155, /*hidden argument*/NULL);
		V_22 = L_156;
		float L_157 = (&V_22)->get_y_2();
		Transform_t3275118058 * L_158 = V_0;
		NullCheck(L_158);
		Vector3_t2243707580  L_159 = Transform_get_position_m1104419803(L_158, /*hidden argument*/NULL);
		V_23 = L_159;
		float L_160 = (&V_23)->get_z_3();
		G_B22_0 = L_157;
		G_B22_1 = L_154;
		G_B22_2 = __this;
		if ((!(((float)L_160) > ((float)(0.0f)))))
		{
			G_B23_0 = L_157;
			G_B23_1 = L_154;
			G_B23_2 = __this;
			goto IL_0480;
		}
	}
	{
		Transform_t3275118058 * L_161 = V_0;
		NullCheck(L_161);
		Vector3_t2243707580  L_162 = Transform_get_position_m1104419803(L_161, /*hidden argument*/NULL);
		V_24 = L_162;
		float L_163 = (&V_24)->get_z_3();
		Transform_t3275118058 * L_164 = V_0;
		NullCheck(L_164);
		Vector3_t2243707580  L_165 = Transform_get_localScale_m3074381503(L_164, /*hidden argument*/NULL);
		V_25 = L_165;
		float L_166 = (&V_25)->get_z_3();
		G_B24_0 = ((float)((float)L_163+(float)((float)((float)L_166/(float)(2.0f)))));
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_04a5;
	}

IL_0480:
	{
		Transform_t3275118058 * L_167 = V_0;
		NullCheck(L_167);
		Vector3_t2243707580  L_168 = Transform_get_position_m1104419803(L_167, /*hidden argument*/NULL);
		V_26 = L_168;
		float L_169 = (&V_26)->get_z_3();
		Transform_t3275118058 * L_170 = V_0;
		NullCheck(L_170);
		Vector3_t2243707580  L_171 = Transform_get_localScale_m3074381503(L_170, /*hidden argument*/NULL);
		V_27 = L_171;
		float L_172 = (&V_27)->get_z_3();
		G_B24_0 = ((float)((float)L_169-(float)((float)((float)L_172/(float)(2.0f)))));
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_04a5:
	{
		Vector3_t2243707580  L_173;
		memset(&L_173, 0, sizeof(L_173));
		Vector3__ctor_m2638739322(&L_173, G_B24_2, G_B24_1, G_B24_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_174 = V_0;
		NullCheck(L_174);
		Vector3_t2243707580  L_175 = Transform_get_localScale_m3074381503(L_174, /*hidden argument*/NULL);
		V_28 = L_175;
		float L_176 = (&V_28)->get_x_1();
		float L_177 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_178 = fabsf(L_177);
		Vector3_t2243707580  L_179;
		memset(&L_179, 0, sizeof(L_179));
		Vector3__ctor_m2638739322(&L_179, L_176, (1.0f), L_178, /*hidden argument*/NULL);
		NullCheck(G_B24_3);
		TheStack_CreateRubble_m2807183450(G_B24_3, L_173, L_179, /*hidden argument*/NULL);
		Transform_t3275118058 * L_180 = V_0;
		Vector3_t2243707580 * L_181 = __this->get_address_of_lastTilePosition_22();
		float L_182 = L_181->get_x_1();
		int32_t L_183 = __this->get_scoreCount_14();
		float L_184 = V_19;
		Vector3_t2243707580 * L_185 = __this->get_address_of_lastTilePosition_22();
		float L_186 = L_185->get_z_3();
		Vector3_t2243707580  L_187;
		memset(&L_187, 0, sizeof(L_187));
		Vector3__ctor_m2638739322(&L_187, L_182, (((float)((float)L_183))), ((float)((float)L_184-(float)((float)((float)L_186/(float)(2.0f))))), /*hidden argument*/NULL);
		NullCheck(L_180);
		Transform_set_localPosition_m1026930133(L_180, L_187, /*hidden argument*/NULL);
	}

IL_0500:
	{
		int32_t L_188 = __this->get_combo_15();
		__this->set_combo_15(((int32_t)((int32_t)L_188+(int32_t)1)));
		Transform_t3275118058 * L_189 = V_0;
		Vector3_t2243707580 * L_190 = __this->get_address_of_lastTilePosition_22();
		float L_191 = L_190->get_x_1();
		int32_t L_192 = __this->get_scoreCount_14();
		Vector3_t2243707580 * L_193 = __this->get_address_of_lastTilePosition_22();
		float L_194 = L_193->get_z_3();
		Vector3_t2243707580  L_195;
		memset(&L_195, 0, sizeof(L_195));
		Vector3__ctor_m2638739322(&L_195, L_191, (((float)((float)L_192))), L_194, /*hidden argument*/NULL);
		NullCheck(L_189);
		Transform_set_localPosition_m1026930133(L_189, L_195, /*hidden argument*/NULL);
	}

IL_0536:
	{
		bool L_196 = __this->get_isMovingOfX_19();
		G_B27_0 = __this;
		if (!L_196)
		{
			G_B28_0 = __this;
			goto IL_0556;
		}
	}
	{
		Transform_t3275118058 * L_197 = V_0;
		NullCheck(L_197);
		Vector3_t2243707580  L_198 = Transform_get_localPosition_m2533925116(L_197, /*hidden argument*/NULL);
		V_29 = L_198;
		float L_199 = (&V_29)->get_x_1();
		G_B29_0 = L_199;
		G_B29_1 = G_B27_0;
		goto IL_0565;
	}

IL_0556:
	{
		Transform_t3275118058 * L_200 = V_0;
		NullCheck(L_200);
		Vector3_t2243707580  L_201 = Transform_get_localPosition_m2533925116(L_200, /*hidden argument*/NULL);
		V_30 = L_201;
		float L_202 = (&V_30)->get_z_3();
		G_B29_0 = L_202;
		G_B29_1 = G_B28_0;
	}

IL_0565:
	{
		NullCheck(G_B29_1);
		G_B29_1->set_secondaryPosition_18(G_B29_0);
		bool L_203 = __this->get_isMovingOfX_19();
		__this->set_isMovingOfX_19((bool)((((int32_t)L_203) == ((int32_t)0))? 1 : 0));
		return (bool)1;
	}
}
// System.Void TheStack::ColorMesh(UnityEngine.Mesh)
extern "C"  void TheStack_ColorMesh_m995884212 (TheStack_t568733713 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_ColorMesh_m995884212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	Color32U5BU5D_t30278651* V_1 = NULL;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	{
		Mesh_t1356156583 * L_0 = ___mesh0;
		NullCheck(L_0);
		Vector3U5BU5D_t1172311765* L_1 = Mesh_get_vertices_m626989480(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3U5BU5D_t1172311765* L_2 = V_0;
		NullCheck(L_2);
		V_1 = ((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		int32_t L_3 = __this->get_scoreCount_14();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = sinf(((float)((float)(((float)((float)L_3)))*(float)(0.25f))));
		V_2 = L_4;
		V_3 = 0;
		goto IL_0085;
	}

IL_002a:
	{
		Color32U5BU5D_t30278651* L_5 = V_1;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		Color32U5BU5D_t30278651* L_7 = __this->get_gameColors_3();
		NullCheck(L_7);
		Color32U5BU5D_t30278651* L_8 = __this->get_gameColors_3();
		NullCheck(L_8);
		Color32U5BU5D_t30278651* L_9 = __this->get_gameColors_3();
		NullCheck(L_9);
		Color32U5BU5D_t30278651* L_10 = __this->get_gameColors_3();
		NullCheck(L_10);
		float L_11 = V_2;
		Color32_t874517518  L_12 = TheStack_Lerp4_m723530364(__this, (*(Color32_t874517518 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Color32_t874517518 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), (*(Color32_t874517518 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (*(Color32_t874517518 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))), L_11, /*hidden argument*/NULL);
		(*(Color32_t874517518 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_12;
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0085:
	{
		int32_t L_14 = V_3;
		Vector3U5BU5D_t1172311765* L_15 = V_0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_002a;
		}
	}
	{
		Mesh_t1356156583 * L_16 = ___mesh0;
		Color32U5BU5D_t30278651* L_17 = V_1;
		NullCheck(L_16);
		Mesh_set_colors32_m1066151745(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color32 TheStack::Lerp4(UnityEngine.Color32,UnityEngine.Color32,UnityEngine.Color32,UnityEngine.Color32,System.Single)
extern "C"  Color32_t874517518  TheStack_Lerp4_m723530364 (TheStack_t568733713 * __this, Color32_t874517518  ___a0, Color32_t874517518  ___b1, Color32_t874517518  ___c2, Color32_t874517518  ___d3, float ___t4, const MethodInfo* method)
{
	{
		float L_0 = ___t4;
		if ((!(((float)L_0) < ((float)(0.33f)))))
		{
			goto IL_002b;
		}
	}
	{
		Color32_t874517518  L_1 = ___a0;
		Color_t2020392075  L_2 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Color32_t874517518  L_3 = ___b1;
		Color_t2020392075  L_4 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		float L_5 = ___t4;
		Color_t2020392075  L_6 = Color_Lerp_m3323752807(NULL /*static, unused*/, L_2, L_4, ((float)((float)L_5/(float)(0.33f))), /*hidden argument*/NULL);
		Color32_t874517518  L_7 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_002b:
	{
		float L_8 = ___t4;
		if ((!(((float)L_8) < ((float)(0.66f)))))
		{
			goto IL_005c;
		}
	}
	{
		Color32_t874517518  L_9 = ___b1;
		Color_t2020392075  L_10 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Color32_t874517518  L_11 = ___c2;
		Color_t2020392075  L_12 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		float L_13 = ___t4;
		Color_t2020392075  L_14 = Color_Lerp_m3323752807(NULL /*static, unused*/, L_10, L_12, ((float)((float)((float)((float)L_13-(float)(0.33f)))/(float)(0.33f))), /*hidden argument*/NULL);
		Color32_t874517518  L_15 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_005c:
	{
		Color32_t874517518  L_16 = ___c2;
		Color_t2020392075  L_17 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Color32_t874517518  L_18 = ___d3;
		Color_t2020392075  L_19 = Color32_op_Implicit_m889975790(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_20 = ___t4;
		Color_t2020392075  L_21 = Color_Lerp_m3323752807(NULL /*static, unused*/, L_17, L_19, ((float)((float)((float)((float)L_20-(float)(0.66f)))/(float)(0.66f))), /*hidden argument*/NULL);
		Color32_t874517518  L_22 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void TheStack::EndGame()
extern "C"  void TheStack_EndGame_m2402922789 (TheStack_t568733713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TheStack_EndGame_m2402922789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2247021248, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_scoreCount_14();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_2 = __this->get_scoreCount_14();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2247021248, L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		__this->set_gameOver_20((bool)1);
		GameObject_t1756533147 * L_3 = __this->get_endPanel_5();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_theStack_11();
		int32_t L_5 = __this->get_stackIndex_13();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_t1756533147 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210(L_7, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t4233889191_m2571400210_MethodInfo_var);
		return;
	}
}
// System.Void TheStack::onButtonClick(System.String)
extern "C"  void TheStack_onButtonClick_m921021571 (TheStack_t568733713 * __this, String_t* ___sceneName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___sceneName0;
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
